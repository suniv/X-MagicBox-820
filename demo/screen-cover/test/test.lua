module(...,package.seeall)
require"socket"
require"pm"
pm.wake("test")

local todo ={
    net = false,--联网
    gsensor = false,
    sht30 = false,
    key = false,
    uart = false,
}

--测试联网
sys.taskInit(function ()
    while not socket.isReady() do sys.wait(1000) end
    local c = socket.tcp()
    while not c:connect("lbsmqtt.airm2m.com", 1884) do sys.wait(2000) end
    c:close()
    todo.net = true
end)

--测试i2c
sys.taskInit(function ()
    local i2cid = 2
    if i2c.setup(i2cid,i2c.SLOW) ~= i2c.SLOW then
        print("i2c.init fail")
        return
    end
    i2c.send(i2cid,0x18,{0x1E,0x05})
    i2c.send(i2cid,0x18,0x57)
    local sl_val = i2c.recv(i2cid,0x18,1)
    sl_val = bit.bor(string.byte(sl_val),0x40)
    i2c.send(i2cid,0x18,{0x57,sl_val})
    i2c.send(i2cid,0x18,0x57)
    if i2c.recv(i2cid,0x18,1):byte() == sl_val then
        todo.gsensor = true
    end

    sys.wait(100)
    i2c.send(i2cid, 0x44, {0x2c, 0x06})
    sys.wait(10) -- 等待采集
    local r = i2c.recv(i2cid, 0x44, 6)
    if #r == 6 and r ~= string.rep(string.char(0),6) then
        todo.sht30 = true
    end
end)


local keyMap = {{},{},{},{},{},{},{},{},{},{},{},}
keyMap[0] = {}
keyMap[255] = {}
keyMap[255][255] = "PWR"
keyMap[2][0] = "1"
keyMap[2][1] = "2"
keyMap[1][0] = "3"
keyMap[1][1] = "4"
keyMap[0][6] = "5"

--注册按键消息的处理函数
rtos.on(rtos.MSG_KEYPAD,function(msg)
    if keyMap[msg.key_matrix_row][msg.key_matrix_col] then
        sys.publish("KEY_EVENT"..keyMap[msg.key_matrix_row][msg.key_matrix_col],msg.pressed)
    end
end)
--初始化键盘阵列
rtos.init_module(rtos.MOD_KEYPAD,0,0x7F,0x7F)

require"common"
local function show(l1,l2)
    l1 = common.utf8ToGb2312(l1)
    l2 = common.utf8ToGb2312(l2)
    disp.clear()
    disp.puttext(l1,0,0)
    disp.puttext(l2,0,20)
    disp.update()
end

sys.taskInit(function ()
    --sys.wait(5000)
    local function write(s)
        uart.write(1,s)
        log.info("uart.write",s)
    end
    local recv = ""
    uart.on(1,"receive",function ()
        local data = ""
        while true do
            data = uart.read(1,"*l")
            if not data or string.len(data) == 0 then break end
            --打开下面的打印会耗时
            log.info("uart.read hex",data,data:len())
            recv = data
        end
    end)
    uart.setup(1,115200,8,uart.PAR_NONE,uart.STOP_1,nil,1)
    uart.set_rs485_oe(1,19)
    local ran = tostring(math.random(10000,99999))
    write(ran)
    sys.wait(2000)
    todo.uart = recv == ran
end)

sys.taskInit(function ()
    show("正在检测内置卡","请稍候")
    ril.request("AT+SETLOCK=1,0,1")--禁止联网
    log.info("============", "============")
    if nvm.get("simTested") == 0 then
        sim.setId(1)
        nvm.set("simTested",1)
        sys.restart("change sim to 1")
        return
    elseif nvm.get("simTested") == 1 then
        local _,d = sys.waitUntil("SIM_IND")
        local pass
        if d == "RDY" then
            for i=1,50 do
                if sim.getIccid() then
                    pass = true
                    break
                end
                sys.wait(100)
            end
        end
        if not pass then
            disp.setbkcolor(0xf800)
            show("卡检测失败","测试结束")
            return
        end
        log.info("sim ccid", sim.getIccid())
        sim.setId(0)
        nvm.set("simccid",sim.getIccid())
        nvm.set("simTested",2)
        sys.restart("change sim to 0")
        return
    elseif nvm.get("simTested") == 2 then
        ril.request("AT+SETLOCK=1,0,8,34,38,39,41")--允许联网
    end

    for i=1,5 do
        show("最上方从左往右数","请按一下第"..i.."个键")
        sys.waitUntil("KEY_EVENT"..i)
    end
    show("请按一下开机键","下方右边的按键")
    sys.waitUntil("KEY_EVENTPWR")
    todo.key = true

    local cnt = 0
    while not todo.net and cnt < 60 do
        disp.clear()
        disp.puttext(common.utf8ToGb2312("联网测试中..."),0,0)
        disp.update()
        sys.wait(1000)
        cnt = cnt + 1
    end

    local result = true
    local notPass = {}
    for i,j in pairs(todo) do
        if not j then
            result = nil
            table.insert(notPass,i)
        end
    end

    require"http"
    require"misc"
    require"http"
    if result then
        http.request("GET", "http://iot.lovemcu.cn/820_1?imei="..misc.getImei().."&iccid="..nvm.get("simccid"), nil, nil, nil, 10000,
        function (result,prompt,head,body)
            sys.publish("HTTP_TEST_IND",result)
        end)
        local r,rr = sys.waitUntil("HTTP_TEST_IND",12000)
        result = r and rr
    end

    disp.setbkcolor(result and 0x07E0 or 0xf800)
    disp.clear()
    if result then
        disp.puttext(common.utf8ToGb2312("测试通过"),0,0)
        nvm.set("test",true)
    else
        disp.puttext(common.utf8ToGb2312("有项目未通过"),0,0)
        local line = 0
        for i=1,#notPass do
            line = line + 20
            disp.puttext(notPass[i],0,line)
        end
    end
    disp.update()
end)
