module(...,package.seeall)


function update()
    disp.setbkcolor(0xffff)
    disp.setcolor(0)
    disp.clear()
    local width, data = qrencode.encode('http://chenxuuu.gitee.io/lua-online/remote.html?'..misc.getImei())
    disp.putqrcode(data, width, 240, 5, 5)
    disp.setbkcolor(0)
    disp.setcolor(0xffff)
end


--需要订阅的主题
local subscribeTopics
--请按需求来更改订阅的主题内容
sys.subscribe("IMEI_READY_IND",function ()--确保imei可被读到
    subscribeTopics = {
        ["magicbox/control/"..misc.getImei()] = 0,--qos为0
    }
end)
--待发送数据缓冲区
local toSend = {}
sys.subscribe("MQTT_SEND_DATA_REMOTE",function (t,data)
    collectgarbage("collect")
    table.insert(toSend,{
        topic = "magicbox/remote/"..misc.getImei().."/"..t,
        payload = data
    })
    log.info("remote.send", #data)
    sys.publish("MQTT_SEND_DATA_IND_REMOTE")
end)
sys.taskInit(function()
    local r, data
    while true do
        while not socket.isReady() do sys.wait(1000) end
        local mqttc = mqtt.client(misc.getImei().."_remoteControl", 300, "user", "password")--心跳时间300秒，用户名密码为默认
        while not mqttc:connect("lbsmqtt.airm2m.com", 1884) do sys.wait(2000) end
        if mqttc:subscribe(subscribeTopics) then--订阅主题
            while true do
                r, data = mqttc:receive(120000, "MQTT_SEND_DATA_IND_REMOTE")
                if r then
                    log.info("mqtt receive", data.topic,data.payload)
                    sys.publish("MQTT_RECEIVE_REMOTE",data)
                elseif data == "MQTT_SEND_DATA_IND_REMOTE" then
                    local ar
                    while #toSend > 0 do
                        local mdata = table.remove(toSend,1)
                        collectgarbage("collect")
                        local mr = mqttc:publish(mdata.topic, mdata.payload)
                        collectgarbage("collect")
                        if not mr then ar = true break end
                    end
                    if ar then break end
                elseif data == "timeout" then
                    --没必要处理这东西
                else
                    break
                end
            end
        end
        r, data = nil,nil
        mqttc:disconnect()
    end
end)

local payloads = {
    screen = function ()--获取屏幕数据
        collectgarbage("collect")
        local buff = {}
        local stream = zlib.deflate(function (data)
            table.insert(buff,data)
        end)
        stream:write(disp.getframe())
        stream:close()
        collectgarbage("collect")
        buff = table.concat(buff)
        collectgarbage("collect")
        local max = 20000
        if #buff < max then
            sys.publish("MQTT_SEND_DATA_REMOTE","screen",buff)
        else
            for i=1,math.floor(#buff/max)+1 do
                sys.publish("MQTT_SEND_DATA_REMOTE","screen"..i,buff:sub((i-1)*max+1,i*max))
            end
            sys.publish("MQTT_SEND_DATA_REMOTE","screen_end","1")
        end
        collectgarbage("collect")
    end
}

-- 收到mqtt数据的处理
sys.subscribe("MQTT_RECEIVE_REMOTE",function (params)
    if payloads[params.payload] then
        payloads[params.payload]()
    else
        sys.publish("KEY_EVENT",params.payload,true)
    end
end)
