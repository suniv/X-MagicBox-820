module(...,package.seeall)

--当前的开关状态
local enable = false

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("摄像头",120,lcd.gety(0),255,255,255)

    lcd.putStringCenter("没画面",120,lcd.gety(3),255,150,255)
    lcd.putStringCenter("请接上摄像头",120,lcd.gety(5),255,0,0)

    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8

    --lcd.putStringCenter("",120,lcd.gety(3),100,100,100)
end

local keyEvents = {
    ["3"] = function ()

    end,
}
keyEvents["A"] = keyEvents["3"]
keyEvents["OK"] = keyEvents["3"]

function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        --page.update()
    end
end


function close()
    disp.camerapreviewclose()
    disp.cameraclose()
    pm.sleep("testTakePhoto")
end

function open()
    pm.wake("testTakePhoto")
    disp.cameraopen(1,0,0,1)
    disp.camerapreviewzoom(-2)
    disp.camerapreview(0,0,0,0,240,240)
end
