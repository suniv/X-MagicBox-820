module(...,package.seeall)
require"mqtt"
local timer_id

--开启mqtt
local enable

--缓存mqtt数据
local mqttTemp = {}
local MAX_COL,MAX_ROW = 240/lcd.CHAR_WIDTH,240/lcd.CHAR_WIDTH/2-5
local function cropTemp() while #mqttTemp > MAX_ROW do table.remove(mqttTemp,1) end end
local function addLine(s,send)
    s = s:gsub("\r","")
    local t = os.date("*t")
    s = string.format("[%02d:%02d:%02d]"..
        (send and string.char(0xB7,0xA2) or string.char(0xCA,0xD5))..":",t.hour,t.min,t.sec)..s
    s = s:split("\n")
    for i=1,#s do
        while true do--截断字符串直到小于一行
            cropTemp()
            if s[i]:len() > MAX_COL then
                table.insert(mqttTemp,{s[i]:sub(1,MAX_COL),send})
                s[i] = s[i]:sub(MAX_COL+1)
            else
                table.insert(mqttTemp,{s[i],send})
                break
            end
        end
    end
    cropTemp()
    page.update()
end


function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("MQTT测试",120,lcd.gety(0),255,255,255)
    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8

    lcd.text("<",lcd.getx(24),lcd.gety(0),255,255,255)
    lcd.text(enable and "开启" or "关闭",lcd.getx(25),lcd.gety(0),100,100,255)
    lcd.text(">",lcd.getx(29),lcd.gety(0),255,255,255)
    lcd.text("按A发送",lcd.getx(23),lcd.gety(1),100,100,100)

    lcd.text("服务器 lbsmqtt.airm2m.com:1884",0,lcd.gety(2),255,200,150)
    lcd.putStringCenter("订阅:"..(misc.getImei() or "").."/luatos",120,lcd.gety(3),124,252,0)
    lcd.putStringCenter("推送:"..(misc.getImei() or "").."/upload",120,lcd.gety(4),138,43,226)

    disp.drawrect(0,lcd.gety(5),239,239,lcd.rgb(25,25,25))
    for i=1,#mqttTemp do
        if mqttTemp[i][2] then
            disp.setcolor(lcd.rgb(100,100,255))
            disp.puttext(mqttTemp[i][1],0,lcd.gety(i+4))
        else
            disp.setcolor(lcd.rgb(0,255,0))
            disp.puttext(mqttTemp[i][1],0,lcd.gety(i+4))
        end
    end
end

local keyEvents = {
    A = function ()
        if enable then sys.publish("MQTT_SEND_DATA",tostring(os.time())) end
    end,
    LEFT = function ()
        enable = not enable
        if not enable then sys.publish("MQTT_SEND_DATA_IND") end
    end,
    RIGHT = function ()
        enable = not enable
        if not enable then sys.publish("MQTT_SEND_DATA_IND") end
    end,
}
keyEvents["3"] = keyEvents.A
keyEvents["4"] = keyEvents.LEFT
keyEvents["5"] = keyEvents.RIGHT

function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end

function open()
    --timer_id = sys.timerLoopStart(page.update,100)
end

function close()
    -- if timer_id then
    --     sys.timerStop(timer_id)
    --     timer_id = nil
    -- end
end

--需要订阅的主题
local subscribeTopics
--请按需求来更改订阅的主题内容
sys.subscribe("IMEI_READY_IND",function ()--确保imei可被读到
    subscribeTopics = {
        [misc.getImei().."/luatos"] = 0,--qos为0
    }
end)
--待发送数据缓冲区
local toSend = {}
sys.subscribe("MQTT_SEND_DATA",function (data)
    table.insert(toSend,{
        topic = (misc.getImei() or "").."/upload",
        payload = data
    })
    sys.publish("MQTT_SEND_DATA_IND")
end)
sys.taskInit(function()
    local r, data
    while true do
        while not socket.isReady() do sys.wait(1000) end
        local mqttc = mqtt.client(misc.getImei().."mqttShow", 300, "user", "password")--心跳时间300秒，用户名密码为默认
        while not enable do sys.wait(1000) end--没开启mqtt
        while not mqttc:connect("lbsmqtt.airm2m.com", 1884) do sys.wait(2000) end
        if mqttc:subscribe(subscribeTopics) then--订阅主题
            sys.publish("MQTT_CONNECTED")
            while true do
                r, data = mqttc:receive(120000, "MQTT_SEND_DATA_IND")
                if r then
                    log.info("mqtt receive", data.topic,data.payload)
                    sys.publish("MQTT_RECEIVE",data)
                elseif data == "MQTT_SEND_DATA_IND" then
                    if not enable then break end--强制断开mqtt连接
                    local ar
                    while #toSend > 0 do
                        local mdata = table.remove(toSend,1)
                        log.info("mqtt send",mdata.topic,mdata.payload)
                        local mr = mqttc:publish(mdata.topic, mdata.payload)
                        if not mr then ar = true break else addLine(mdata.payload,true) end
                    end
                    if ar then break end
                elseif data == "timeout" then
                    --没必要处理这东西
                else
                    break
                end
            end
        end
        sys.publish("MQTT_DISCONNECTED")
        r, data = nil,nil
        mqttc:disconnect()
    end
end)

--mqtt服务器连接上之后调用的代码
sys.subscribe("MQTT_CONNECTED",function ()
    addLine(common.utf8ToGb2312("服务器已连接"))
    log.info("mqttTask", "server connected!")
end)

--mqtt服务器断开之后调用的代码
sys.subscribe("MQTT_DISCONNECTED",function ()
    addLine(common.utf8ToGb2312("服务器已断开"))
    log.info("mqttTask", "server disconnected!")
end)

-- 收到mqtt数据的处理
sys.subscribe("MQTT_RECEIVE",function (params)
    addLine(params.payload)
end)
