--- 模块功能：经典蓝牙示例
-- @author Darren Cheng
-- @module bluetooth.bt
-- @license MIT
-- @copyright openLuat
-- @release 2021.08.24
-- @注意 需要使用core(Luat_VXXXX_RDA8910_BT_FLOAT)版本
module(..., package.seeall)
require "audio"

local vol = 50
local musicstatus = 1
local keyMap = {{},{},{},{},{},{},{},{},{},{}}
keyMap[0] = {}
keyMap[255] = {}
keyMap[2][0] = "ENTER"
keyMap[2][1] = "DOWN"
keyMap[1][0] = "UP"
keyMap[1][1] = "ESC"
keyMap[255][255] = "PWK"

local function keyMsg(msg)
    if keyMap[msg.key_matrix_row][msg.key_matrix_col] == "PWK" and msg.pressed then
        btcore.setavrcpsongs(musicstatus)
        if musicstatus == 0 then
            musicstatus = 1
        elseif musicstatus == 1 then
            musicstatus = 0
        end
        log.info("bt", "musicstatus",musicstatus)
    end
    if msg.pressed then
        if keyMap[msg.key_matrix_row][msg.key_matrix_col] == "ENTER" then
            btcore.setavrcpsongs(3)
            log.info("bt","下一曲")
        elseif keyMap[msg.key_matrix_row][msg.key_matrix_col] == "ESC" then
            btcore.setavrcpsongs(2)
            log.info("bt","上一曲")
        elseif keyMap[msg.key_matrix_row][msg.key_matrix_col] == "UP" then
            vol = vol + 10
            if vol > 127 then
                vol = 127
            end
            btcore.setavrcpvol(vol)
            log.info("bt","加音量", vol)
        elseif keyMap[msg.key_matrix_row][msg.key_matrix_col] == "DOWN" then
            vol = vol - 10
            if vol < 0 then
                vol = 0
            end
            btcore.setavrcpvol(vol)
            log.info("bt","减音量", vol)
        end
    end
end
rtos.on(rtos.MSG_KEYPAD,keyMsg)
rtos.init_module(rtos.MOD_KEYPAD,0,0x7F,0x7F)

local function init()
    log.info("bt", "init")
    rtos.on(rtos.MSG_BLUETOOTH, function(msg)
        if msg.event == btcore.MSG_OPEN_CNF then
            sys.publish("BT_OPEN", msg.result) --蓝牙打开成功
        elseif msg.event == btcore.MSG_BT_HFP_CONNECT_IND then
            sys.publish("BT_HFP_CONNECT_IND", msg.result) --hfp连接成功
		elseif msg.event == btcore.MSG_BT_HFP_DISCONNECT_IND then
            log.info("bt", "bt hfp disconnect") --hfp断开连接
        elseif msg.event == btcore.MSG_BT_HFP_CALLSETUP_OUTGOING then
            log.info("bt", "bt call outgoing") --建立呼出电话
        elseif msg.event == btcore.MSG_BT_HFP_CALLSETUP_INCOMING then
            log.info("bt", "bt call incoming") --呼叫传入    
            sys.publish("BT_CALLSETUP_INCOMING", msg.result)
        elseif msg.event == btcore.MSG_BT_HFP_RING_INDICATION then
            log.info("bt", "bt ring indication") --呼叫传入铃声
        elseif msg.event == btcore.MSG_BT_AVRCP_CONNECT_IND then
            sys.publish("BT_AVRCP_CONNECT_IND", msg.result) --avrcp连接成功
		elseif msg.event == btcore.MSG_BT_AVRCP_DISCONNECT_IND then
            log.info("bt", "bt avrcp disconnect") --avrcp断开连接
        end
    end)
end

sys.taskInit(function()
    sys.wait(5000)
    -- audio.setChannel(1) -- 可调用此api切换播放通道，默认spk
    init()  -- 初始化

    log.info("bt", "poweron")
    btcore.open(2)  --打开经典蓝牙
    sys.waitUntil("BT_OPEN", 5000) --等待蓝牙打开成功

    log.info("bt", "设置蓝牙参数")
    btcore.setname("Cat1BT")-- 设置广播名称
    btcore.setvisibility(0x11)-- 设置蓝牙可见性
    log.info("bt", "蓝牙可见性",btcore.getvisibility())
    
    local _, result = sys.waitUntil("BT_AVRCP_CONNECT_IND") --等待连接成功
    if result ~= 0 then
        return
    end
    log.info("bt", "连接成功")
    while true do
        vol = btcore.getavrcpvol()
        if vol == -1 then
            log.info("bt", "获取音量失败", vol)
        elseif vol == -2  then
            log.info("bt", "设备不支持获取音量", vol)
        else
            log.info("bt", "设备音量", vol)
        end
        sys.wait(1000)
    end
end)


