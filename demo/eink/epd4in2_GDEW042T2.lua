module(...,package.seeall)

require"misc"
require"utils"
require"pins"

--电压域
pmd.ldoset(15,pmd.LDO_VLCD)

log.info("spi.setup",spi.setup(spi.SPI_1,0,0,8,13000000,0,0))

local function getBusyFnc(msg)
    log.info("Gpio.getBusyFnc",msg)
    if msg==cpu.INT_GPIO_POSEDGE then--上升沿中断
        --不动作
    else--下降沿中断
        sys.publish("BUSY_DOWN")
    end
end

--初始化三个控制引脚
local getBusy         = pins.setup(7,getBusyFnc)
local setRST          = pins.setup(12,1)
local setDC           = pins.setup(18,1)

-- Display resolution
EPD_WIDTH       = 400
EPD_HEIGHT      = 300

setDC(1)
local function sendCommand(data)
    --log.info("epd4in2.sendCommand",data)
    setDC(0)
    spi.send(spi.SPI_1,string.char(data))
end

local function sendData(data)
    --log.info("epd4in2.sendData",data)
    setDC(1)
    spi.send(spi.SPI_1,string.char(data))
end

local function sendDataString(data)
    --log.info("epd4in2.sendData",data)
    setDC(1)
    spi.send(spi.SPI_1,data)
end

local function sendDataTrans(data)
    setDC(1)
    local temp
    for i=EPD_WIDTH,1,-1 do
        for j=EPD_HEIGHT/8,1,-1 do
            temp = (j-1)*EPD_WIDTH+i
            spi.send(spi.SPI_1,data:sub(temp,temp))
        end
    end
end

local function wait()
    repeat
        sendCommand(0x71)
        sys.waitUntil("BUSY_DOWN",100)
        log.info("lcd busy")
    until getBusy() == 1
end

local function reset()
    log.info("epd4in2.reset","")
    setRST(0)
    sys.wait(500)
    setRST(1)
    sys.wait(500)
end

function deepSleep()
    log.info("epd4in2.deepSleep","")
    sendCommand(0X50)--  //VCOM AND DATA INTERVAL SETTING
    sendData(0xf7)-- //WBmode:VBDF 17|D7 VBDW 97 VBDB 57		WBRmode:VBDF F7 VBDW 77 VBDB 37  VBDR B7

    sendCommand(0X02)--  	//power off
    wait()--waiting for the electronic paper IC to release the idle signal
    sendCommand(0X07)--  	//deep sleep
    sendData(0xA5)
    setDC(0)
end

function init()
    log.info("epd4in2.init","")
    reset()

    sendCommand(0x06)--         //boost soft start
    sendData(0x17)--   //A
    sendData(0x17)--   //B
    sendData(0x17)--   //C

    sendCommand(0x04)--  //Power on
    wait()-- //waiting for the electronic paper IC to release the idle signal

    sendCommand(0x00)--     //panel setting
    sendData(0x1f)--    //LUT from OTP
    sendData(0x0d)--    //VCOM to 0V
    log.info("epd4in2.init","done")
end

function display_frame()
    log.info("epd4in2.display_frame","start")
    sendCommand(0x12)
    sys.wait(5)
    wait()
    log.info("epd4in2.display_frame","done")
end

function clear(color)
    sendCommand(0x10)--Transfer old data
    sendDataString(string.rep(string.char(color),EPD_WIDTH*EPD_HEIGHT/8))
    sendCommand(0x13)--Transfer new data
    sendDataString(string.rep(string.char(color),EPD_WIDTH*EPD_HEIGHT/8))
    display_frame()
    log.info("epd4in2.clear","done")
end

--输入值：数组
function showPictureN(pic)
    wait()
    log.info("epd4in2.showPicture","")
    sendCommand(0x10)
    for i=1,#pic do
        sendData(pic[i])
    end
    sendCommand(0x13)
    for i=1,#pic do
        sendData(pic[i])
    end
    display_frame()
end

--输入值：string
function showPicture(pic)
    wait()
    log.info("epd4in2.showPicture","")
    sendCommand(0x10)
    sendDataString(pic)
    sendCommand(0x13)
    sendDataString(pic)
    display_frame()
end

--输入值：string,按页显示的数据
function showPicturePage(pic)
    wait()
    log.info("epd4in2.showPicturePage","")
    sendCommand(0x10)
    sendDataTrans(pic)
    sendCommand(0x13)
    sendDataTrans(pic)
    display_frame()
end


